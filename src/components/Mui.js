import React, { useState } from "react";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import PhotoCamera from "@mui/icons-material/PhotoCamera";
import DeleteIcon from "@mui/icons-material/Delete";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function Mui() {
  const [trigger, setTrigger] = useState(false);
  const [username, setUsername] = useState({
    name: "",
    password: "",
  });
  const data = [];

  // tambah 1 input password
  // masukkan 2 input dalam satu state
  // masukkan data tersebut kedalam data

  return (
    <div>
      <Button
        variant="outlined"
        color="warning"
        size="large"
        // endIcon={<DeleteIcon />}
        onClick={() => setTrigger(true)}
      >
        Click
      </Button>
      <IconButton color="error">
        <PhotoCamera />
      </IconButton>
      <Modal
        open={trigger}
        onClose={() => setTrigger(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Box sx={{ width: "100%" }}>
            <TextField
              fullWidth
              label="username"
              variant="outlined"
              onChange={(e) =>
                setUsername({
                  ...username,
                  name: e.target.value,
                })
              }
            />
            <TextField
              fullWidth
              label="password"
              variant="outlined"
              onChange={(e) =>
                setUsername({
                  ...username,
                  password: e.target.value,
                })
              }
            />
          </Box>
          <Button
            variant="contained"
            color="success"
            onClick={() => {
              data.push(username);
              console.log(data);
            }}
            sx={{ mt: 5, mr: 1 }}
          >
            submit
          </Button>
          <Button
            variant="contained"
            color="error"
            onClick={() => setTrigger(false)}
            sx={{ mt: 5 }}
          >
            Close
          </Button>
        </Box>
      </Modal>
    </div>
  );
}

export default Mui;
