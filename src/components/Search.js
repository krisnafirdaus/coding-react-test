import React, { useState } from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { TextField } from "@mui/material";

function Search() {
  const [state, setState] = useState("");

  const data = [
    {
      id: 1,
      name: "krisna",
      major: "informatic",
    },
    {
      id: 2,
      name: "andi",
      major: "ipa",
    },
    {
      id: 3,
      name: "kusen",
      major: "matematika",
    },
  ];

  const result = data.filter((x) =>
    x.name.toLowerCase().includes(state.toLowerCase())
  );

  return (
    <Box>
      <Grid container>
        {state}
        <Grid xs={12} sx={{ margin: 10 }}>
          <TextField
            label="Search"
            onChange={(e) => {
              setState(e.target.value);
            }}
          />
        </Grid>
        <Grid xs={12} sx={{ margin: 10 }}>
          <table>
            <tr>
              <th>Name</th>
              <th>Major</th>
            </tr>
            {state
              ? result.map((x) => {
                  return (
                    <tr>
                      <td>{x.name}</td>
                      <td>{x.major}</td>
                    </tr>
                  );
                })
              : data.map((x) => {
                  return (
                    <tr>
                      <td>{x.name}</td>
                      <td>{x.major}</td>
                    </tr>
                  );
                })}
          </table>
        </Grid>
      </Grid>
    </Box>
  );
}

export default Search;
