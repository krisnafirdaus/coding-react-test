import React, { useState, useEffect } from "react";

function Timer() {
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);

  // useEffect(() => {
  //   let interval = setInterval(() => {
  //     if (seconds === 59) {
  //       if (minutes === 59) {
  //         setHours(hours + 1);
  //         setMinutes(0);
  //       } else {
  //         setMinutes(minutes + 1);
  //       }
  //       setSeconds(0);
  //     } else {
  //       setSeconds(seconds + 1);
  //     }
  //   }, 1000);

  //   return () => clearInterval(interval);
  // }, [hours, minutes, seconds]);

  // useState
  // useEffect
  // interval

  return (
    <div>
      <p>
        Time elapsed: {hours < 10 ? "0" + hours : hours}:
        {minutes < 10 ? "0" + minutes : minutes}:
        {seconds < 10 ? "0" + seconds : seconds}
      </p>
    </div>
  );
}

export default Timer;
