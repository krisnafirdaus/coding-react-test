import React, { useState } from "react";

function StudentTable({ students, onSave }) {
  const [editingIndex, setEditingIndex] = useState(-1);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [major, setMajor] = useState("");

  const handleAdd = () => {
    onSave([...students, { name, email, major }]);
    setName("");
    setEmail("");
    setMajor("");
  };

  const handleEdit = () => {
    onSave([
      ...students.slice(0, editingIndex),
      { name, email, major },
      ...students.slice(editingIndex + 1),
    ]);
    setEditingIndex(-1);
    setName("");
    setEmail("");
    setMajor("");
  };

  return (
    <div>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Major</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {students.map((student, index) => (
            <tr key={index}>
              <td>{student.name}</td>
              <td>{student.email}</td>
              <td>{student.major}</td>
              <td>
                <button
                  type="button"
                  onClick={() => {
                    setEditingIndex(index);
                    setName(student.name);
                    setEmail(student.email);
                    setMajor(student.major);
                  }}
                >
                  Edit
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      {editingIndex === -1 ? (
        <div>
          <h2>Add Student</h2>
          <div>
            <label htmlFor="name">Name:</label>
            <input
              type="text"
              id="name"
              value={name}
              onChange={(event) => setName(event.target.value)}
            />
          </div>
          <div>
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              id="email"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
            />
          </div>
          <div>
            <label htmlFor="major">Major:</label>
            <input
              type="text"
              id="major"
              value={major}
              onChange={(event) => setMajor(event.target.value)}
            />
          </div>
          <button type="button" onClick={handleAdd}>
            Add Student
          </button>
        </div>
      ) : (
        <div>
          <h2>Edit Student</h2>
          <div>
            <label htmlFor="name">Name:</label>
            <input
              type="text"
              id="name"
              value={name}
              onChange={(event) => setName(event.target.value)}
            />
          </div>
          <div>
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              id="email"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
            />
          </div>
          <div>
            <label htmlFor="major">Major:</label>
            <input
              type="text"
              id="major"
              value={major}
              onChange={(event) => setMajor(event.target.value)}
            />
          </div>
          <button type="button" onClick={handleEdit}>
            Save Changes
          </button>
        </div>
      )}
    </div>
  );
}

export default StudentTable;
