import React, { useState } from "react";

const Homepage = (props) => {
  const [name, setName] = useState("Tombol"); // state initial dari functional component

  const Click = () => {
    setName("Button");
    props.buttonName(false);
  };

  console.log(props.name);

  return (
    <div>
      <h1>{name}</h1>
      <button onClick={Click}>Homepage Button</button>
    </div>
  );
};

export default Homepage;
