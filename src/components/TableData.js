import React, { useState } from "react";

function TableData() {
  const [nilai, setNilai] = useState(0);

  const data = [
    {
      id: 1,
      name: "Krisna",
    },
    {
      id: 2,
      name: "andi",
    },
  ];

  return (
    <div style={{ display: "block" }}>
      {data.map((x) => {
        return (
          <div key={x.id} style={{ display: "flex" }}>
            <h1>{x.id}.</h1>
            <h1>{x.name}</h1>
            <button onClick={() => setNilai(x.id)}>Klik</button>
          </div>
        );
      })}

      <h1>{nilai}</h1>
    </div>
  );
}

export default TableData;
