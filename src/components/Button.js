import React, { Component } from "react";

class Button extends Component {
  // html dan javas
  constructor(props) {
    super(props); // untuk get method dari induk (superclass)
    this.state = {
      name: "Tombol",
    };
  }

  onButtonClick = () => {
    this.setState({ name: "Button" });
  };

  render() {
    return (
      <div>
        <h1>{this.state.name}</h1>
        <button onClick={this.onButtonClick}>Klik</button>
      </div>
    );
  }
}

export default Button;
